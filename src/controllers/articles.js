const _ = require("lodash")
const express = require("express")
const router = express.Router()
const auth = require("../middleware/auth")
const { Article, properties } = require("../models/article")

router.get("/", async (req, res, next) => {
  try {
    console.log(`Getting all articles`)

    const articles = await Article.findAll(
      {
        order: [['createdAt', 'DESC']]
      }
    )

    return res.status(200).send(_.map(articles, article => _.pick(article, properties)))
  }
  catch (ex) {
    next(ex)
  }
})

router.get("/:id", auth, async (req, res, next) => {
  try {
    console.log(`Getting article (id: ${req.params.id})`)

    const article = await Article.findOne({ where: { userId: req.user.id, id: req.params.id } })
    if (!article) {
      return res.status(404).send("The article with the given ID was not found")
    }
    return res.status(200).send(_.pick(article, properties))
  }
  catch (ex) {
    next(ex)
  }
})

router.post("/", auth, async (req, res, next) => {
  try {
    console.log(`Saving the article with userId: ${req.user.id}`)

    let article = req.body
    article.userId = req.user.id
    article = await Article.create(article)

    return res.status(200).send(_.pick(article, properties))
  }
  catch (ex) {
    next(ex)
  }
})

router.patch("/", auth, async (req, res, next) => {
  try {
    console.log(`Updating article (id: ${req.body.id})`)

    //Sequelize has clumsy way for reaching out returned object
    const [rowsAffected, [article]] = await Article.update(req.body,
      {
        where: { userId: req.user.id, id: req.body.id },
        returning: true
      })
    if (!article) {
      return res.status(404).send("The article with the given ID was not found")
    }
    return res.status(200).send(_.pick(article, properties))
  }
  catch (ex) {
    next(ex)
  }
})

router.delete("/", auth, async (req, res, next) => {
  try {
    console.log(`Deleting article (id: ${req.body.id})`)

    const rowsAffected = await Article.destroy({ where: { userId: req.user.id, id: req.body.id } })
    if (!rowsAffected) {
      return res.status(404).send("The article with the given ID was not found")
    }
    return res.status(200).send("Article deleted")
  }
  catch (ex) {
    next(ex)
  }
})

module.exports = router;