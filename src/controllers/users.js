const bcrypt = require("bcryptjs")
const _ = require("lodash")
const express = require("express")
const router = express.Router()
const auth = require("../middleware/auth")
const { User, properties } = require("../models/user")

router.get("/", auth, async (req, res, next) => {
  try {
    console.log(`Getting user with id ${req.user.id}`)

    const user = await User.findByPk(req.user.id)

    return res.status(200).send(_.pick(user, properties))
  }
  catch (ex) {
    next(ex)
  }
})

router.patch("/", auth, async (req, res, next) => {
  try {
    const { firstName, lastName, email } = req.body
    
    console.log(`Updating user ${email} with user id ${req.user.id}`)

    const [rowsAffected, [user]] = await User.update(
      {
        firstName,
        lastName,
        email
      },
      {
        where: { id: req.user.id },
        returning: true
      })

    if (!user) {
      return res.status(404).send("The user with the given ID was not found")
    }
    return res.status(200).send(_.pick(user, properties))
  }
  catch (ex) {
    next(ex)
  }
})

router.patch("/password", auth, async (req, res) => {
  try {
    console.log(`Changing password for user id ${req.user.id}`)

    const salt = await bcrypt.genSalt(10)
    const password = await bcrypt.hash(req.body.password, salt)

    const [rowsAffected, [user]] = await User.update(
      { password },
      {
        where: { id: req.user.id },
        returning: true
      })

    if (!user) {
      return res.status(404).send("The user with the given ID was not found")
    }
    return res.status(200).send(_.pick(user, properties))
  }
  catch (ex) {
    next(ex)
  }
})

module.exports = router
