const bcrypt = require("bcryptjs")
const _ = require("lodash")
const express = require("express")
const router = express.Router()
const { User, properties } = require("../models/user")

//Register new user
router.post("/", async (req, res, next) => {
  try {
    let user = await User.findOne({ where: { email: req.body.email } })
    if (user) {
      return res.status(400).send("User already registered")
    }
    user = new User(req.body)

    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(req.body.password, salt)

    const token = await user.generateAuthToken()
    await user.save()

    return res.status(200)
      .header("x-auth-token", token)
      .send(_.pick(user, properties))
  }
  catch (ex) {
    next(ex)
  }
})

//Fetch current user token
router.post("/me", async (req, res, next) => {
  try {
    const user = await User.findOne({ where: { email: req.body.email } })
    if (!user) {
      return res.status(400).send("Invalid email or password")
    }

    const isValid = await bcrypt.compare(req.body.password, user.password)
    if (!isValid) {
      return res.status(400).send("Invalid email or password")
    }
    const token = await user.generateAuthToken()

    return res.status(200).send(token)
  }
  catch (ex) {
    next(ex)
  }
})

module.exports = router