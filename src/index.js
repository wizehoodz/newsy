require("dotenv").config()
const winston = require('winston')
const express = require("express")
const app = express()
const server = require('http').createServer(app)

require('./startup/logging')()
require("./startup/controllers")(app)
require("./startup/db")
require("./startup/config")()

app.get('/', async (req, res) => {
    try {
        return res.status(200).send("API started!")
    }
    catch (ex) {
        return res.status(500).send(ex.message)
    }
})

const port = process.env.PORT || 3000

server.listen(port, () => {
    winston.info(`Listening on port ${port}...`)
})

module.exports = server
