const config = require("config")
const { Sequelize, DataTypes } = require("sequelize")

const path = process.env.DATABASE_URL || config.get("DATABASE_URL")
if (!path) {
    throw new Error("FATAL ERROR: DATABASE_URL is not defined.")
}

const sequelize = new Sequelize(path, {
    dialect: 'postgres',
    dialectOptions: {
        //Lets make sure we don't get any SSL rejections
        ssl: {
            require: false,
            rejectUnauthorized: false
        }
    },
    //Optional flags
    // ssl: true,
    // keepAlive: true,
    // operatorsAliases: false,
    // logging: true
})

sequelize.authenticate().then(() => {
    console.log('Connection established successfully.')
}).catch(err => {
    console.error('Unable to connect to the database:', err)
})

module.exports = { sequelize, DataTypes }