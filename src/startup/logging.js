const winston = require('winston')
const { format, transports } = winston
const { combine, timestamp, json, simple, colorize } = format
const path = require('path')

module.exports = function () {
    winston.configure({
        transports: [
            new transports.File({
                dirname: path.join(__dirname, 'logs'),
                filename: 'logfile.log',
                format: combine(
                    timestamp(),
                    json()),
            }),
            new transports.Console({
                format: combine(
                    colorize(),
                    simple())
            })
        ],
        exceptionHandlers: [
            new transports.File({
                dirname: path.join(__dirname, 'logs'),
                filename: 'uncaughtExceptions.log',
                format: combine(
                    timestamp(),
                    json()
                ),
            }),
            new transports.Console({
                format: simple()
            })
        ]
    })

    process.on('unhandledRejection', (ex) => {
        throw ex
    })
}