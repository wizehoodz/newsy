const express = require("express")
const cors = require("cors")
const auth = require("../controllers/auth")
const users = require("../controllers/users")
const articles = require("../controllers/articles")
const error = require("../middleware/error")

module.exports = app => {
  app.use(cors())
  app.use(express.json())
  app.use("/api/auth", auth)
  app.use("/api/users", users)
  app.use("/api/articles", articles)
  app.use(error)
}
