//Module to sync db models
require('dotenv').config()
const { User } = require("./user")
const { Article } = require("./article")

//Uncomment following lines for sync
// User.sync({ force: true }).then(() => {
//   console.log('Done syncing.')
// }).catch(err => {
//   console.error('Error syncing:', err)
// })

// Article.sync({ force: true }).then(() => {
//     console.log('Done syncing.')
// }).catch(err => {
//     console.error('Error syncing:', err)
// })
