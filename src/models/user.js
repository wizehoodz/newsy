const jwt = require("jsonwebtoken")
const config = require("config")
const { sequelize, DataTypes } = require("./../startup/db")

const User = sequelize.define('user',
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4(),
      allowNull: false,
      primaryKey: true
    },
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  },
  {
    paranoid: true
  }
)

User.prototype.generateAuthToken = async function () {
  const token = jwt.sign(
    { id: this.id, isAdmin: this.isAdmin },
    process.env.JWT_PRIVATE_KEY || config.get("JWT_PRIVATE_KEY")
  )
  return token
}

//Viewmodel properties
exports.properties = ["id", "firstName", "lastName", "email"]
exports.User = User
