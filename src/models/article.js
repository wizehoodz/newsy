const { sequelize, DataTypes } = require("../startup/db")
const { User } = require("../models/user")

const Article = sequelize.define('article',
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4(),
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    tags: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
    },
    content: {
      type: DataTypes.TEXT(),
      allowNull: false
    },
  },
  {
    paranoid: true
  }
)

Article.belongsTo(User, { foreignKey: "userId" })

exports.properties = Object.keys(Article.rawAttributes)
exports.Article = Article
