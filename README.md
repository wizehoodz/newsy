# newsy

A Node-based backend API that supports a news-related web and mobile application.

## Prerequisites for local environment 

- Install Postgres database

The official instructions for various OS can be found here: [https://www.postgresql.org/download](https://www.postgresql.org/download)

- Install packages
```
npm i 
```

App supports configuration for specific environment. Those are served via:
- ENV file (within _root_):
```
NODE_ENV=
JWT_PRIVATE_KEY=
DATABASE_URL=
```
- or JSON file (within _config/ENVIRONMENT.json_):
```
{
     "JWT_PRIVATE_KEY": "",
     "DATABASE_URL": ""
}
```

## Starting the API 

- Run API directly
```
npm start 
```
OR track development changes as well
```
npm run monitor
```